from lxml import objectify
from lxml import etree
import datetime
import warnings
from utilities import is_xml
from jinja2 import Environment, FileSystemLoader


_xs_date = lambda x: datetime.datetime.strptime(str(x)[:10], "%Y-%m-%d").date()
_bool_from_string = lambda x: bool(int(x)) if x is not None else False
_int_or_float = lambda x: int(x) if int(x) == float(x) else float(x)
_zip_string = lambda x: '0'*(5-len(str(x)))+str(x)

class Form4(object):

    def __init__(self, xml):

        if not is_xml(xml):
            raise Exception('Supplied data is not XML')

        self._check_for_bad_schemas(xml)

        ## remove <XML> </XML> lines from beginning and end
        doctext = xml
        if doctext.upper().startswith('<XML>'):
            doctext = doctext[5:].strip()
        if doctext.upper().endswith('</XML>'):
            doctext = doctext[:-6].strip()

        ## See documentation for info on the schemas
        if '<documentType>4</documentType>' in xml:
            schemafile = "schemas/version5_1/ownership4Document.xsd.xml"
        elif '<documentType>4/A</documentType>' in xml:
            schemafile = "schemas/version5_1/ownership4ADocument.xsd.xml"
        else:
            raise Exception("Unknown document type; no schema available")
        with open(schemafile, 'r') as sf:
            schema = etree.XMLSchema(etree.parse(sf))
        parser = objectify.makeparser(schema=schema)

        self.issuer = {}
        self.reportingOwner = []
        self.footnotes = []
        self.transactions = {}

        warning_errors = [
            "This element is not expected",
            "Character content other than whitespace is not allowed"
        ]

        try:
            self.tree = objectify.fromstring(doctext, parser)
        except etree.XMLSyntaxError as a:
            if any([a.message.find(x) > 0 for x in warning_errors]):
                if "Element 'derivativeSecurity'" in a.message or "Element 'nonDerivativeSecurity'" in a.message:
                    warnings.warn('Older schema, trying to parse')
                else:
                    warnings.warn(a.message + ' Trying to parse anyway')
                try:
                    self.tree = objectify.fromstring(doctext)
                except Exception as e:
                    raise Exception('Could not parse XML')
            else:
                raise a

        self._parse_form4()

    def _check_for_bad_schemas(self, text):
        if any('<schemaVersion>{}</schemaVersion>'.format(x) in text for x in ['X0201', 'X0101']):
            raise Exception('Incompatible schemaVersion: cannot parse')


    def _parse_form4(self):

        elements = {x:self.tree.find(x) for x in [
            'schemaVersion', 'documentType', 'periodOfReport', 'remarks', 'notSubjectToSection16', 'issuer',
            'ownerSignature', 'footnotes'
        ]}

        self.otherdata = {}
        self.otherdata['schemaVersion'] = elements['schemaVersion']
        self.otherdata['documentType'] = elements['documentType'] if etree.iselement(elements['documentType']) else None
        self.otherdata['periodOfReport'] = elements['periodOfReport'] if etree.iselement(elements['periodOfReport']) else None
        self.otherdata['remarks'] = elements['remarks'] if etree.iselement(elements['remarks']) else ""
        self.otherdata['notSubjectToSection16'] = elements['notSubjectToSection16'] if etree.iselement(elements['notSubjectToSection16']) else None
        if self.otherdata['documentType'] == '4/A':
            doos = self.tree.find('dateOfOriginalSubmission')
            self.otherdata['dateOfOriginalSubmission'] = _xs_date(doos) if etree.iselement(doos) else None

        self.issuer = {
            "issuerCik": elements['issuer'].findtext('issuerCik'),
            "issuerName": elements['issuer'].findtext('issuerName'),
            "issuerTradingSymbol": elements['issuer'].findtext('issuerTradingSymbol')
        }

        self.ownerSignature = [
            {
                "signatureName": str(x.signatureName),
                "signatureDate": _xs_date(x.signatureDate)
            }
            for x in elements['ownerSignature']
        ]

        self.footnotes = {
            x.attrib['id']: str(x) for x in elements['footnotes'].findall('footnote')
        } if etree.iselement(elements['footnotes']) else {}

        rpt_instructions = {
            "rptOwnerCik": {"tags": "reportingOwnerId", "footnotes": False},
            "rptOwnerName": {"tags": "reportingOwnerId", "footnotes": False},
            "rptOwnerStreet1": {"tags": "reportingOwnerAddress", "footnotes": False},
            "rptOwnerStreet2": {"tags": "reportingOwnerAddress", "footnotes": False},
            "rptOwnerCity": {"tags": "reportingOwnerAddress", "footnotes": False},
            "rptOwnerState": {"tags": "reportingOwnerAddress", "footnotes": False},
            "rptOwnerZipCode": {"tags": "reportingOwnerAddress", "footnotes": False, "encoding": _zip_string},
            "rptOwnerStateDescription": {"tags": "reportingOwnerAddress", "footnotes": False},
            "isDirector": {"tags": "reportingOwnerRelationship", "footnotes": False, "encoding": _bool_from_string},
            "isOfficer": {"tags": "reportingOwnerRelationship", "footnotes": False, "encoding": _bool_from_string},
            "isTenPercentOwner": {"tags": "reportingOwnerRelationship", "footnotes": False, "encoding": _bool_from_string},
            "isOther": {"tags": "reportingOwnerRelationship", "footnotes": False, "encoding": _bool_from_string},
            "officerTitle": {"tags": "reportingOwnerRelationship", "footnotes": False},
            "otherText": {"tags": "reportingOwnerRelationship", "footnotes": False},
        }

        self.reportingOwner = [self._get_entries(rpt_instructions, r) for r in self.tree.findall('reportingOwner')]

        transaction_instructions = {
            "nonDerivativeHolding": {
                "securityTitle": {"footnotes": True},
                "sharesOwnedFollowingTransaction": {"tags": "postTransactionAmounts", "encoding": _int_or_float},
                "valueOwnedFollowingTransaction": {"tags": "postTransactionAmounts", "encoding": float},
                "directOrIndirectOwnership": {"tags": "ownershipNature"},
                "natureOfOwnership": {"tags": "ownershipNature"},
            },
            "derivativeTransaction": {
                "securityTitle": {"footnotes": True},
                "transactionDate": {"footnotes": True, "encoding": _xs_date},
                "deemedExecutionDate": {"encoding": _xs_date},
                "transactionFormType": {"tags": "transactionCoding", "footnotes": False, "encoding": str},
                "transactionCode": {"tags": "transactionCoding", "footnotes": False, "encoding": str},
                "equitySwapInvolved": {"tags": "transactionCoding", "footnotes": False, "encoding": _bool_from_string},
                "transactionCoding": {},
                "transactionTimeliness": {},
                "transactionShares": {"tags": "transactionAmounts", "encoding": _int_or_float},
                "transactionPricePerShare": {"tags": "transactionAmounts", "encoding": float},
                "transactionAcquiredDisposedCode": {"tags": "transactionAmounts", "required": (1, 1)},
                "sharesOwnedFollowingTransaction": {"tags": "postTransactionAmounts", "encoding": _int_or_float},
                "valueOwnedFollowingTransaction": {"tags": "postTransactionAmounts", "encoding": float},
                "directOrIndirectOwnership": {"tags": "ownershipNature"},
                "natureOfOwnership": {"tags": "ownershipNature"},
                "conversionOrExercisePrice": {"encoding": float},
                "exerciseDate": {"encoding": _xs_date},
                "expirationDate": {"encoding": _xs_date},
                "underlyingSecurityTitle": {"tags": "underlyingSecurity", "encoding": str},
                "underlyingSecurityShares": {"tags": "underlyingSecurity", "encoding": _int_or_float},
                "underlyingSecurityValue": {"tags": "underlyingSecurity", "encoding": float},
            },
            "derivativeHolding": {
                "securityTitle": {"footnotes": True},
                "sharesOwnedFollowingTransaction": {"tags": "postTransactionAmounts", "encoding": _int_or_float},
                "valueOwnedFollowingTransaction": {"tags": "postTransactionAmounts", "encoding": float},
                "directOrIndirectOwnership": {"tags": "ownershipNature"},
                "natureOfOwnership": {"tags": "ownershipNature"},
                "conversionOrExercisePrice": {"encoding": float},
                "exerciseDate": {"encoding": _xs_date},
                "expirationDate": {"encoding": _xs_date},
                "underlyingSecurityTitle": {"tags": "underlyingSecurity", "encoding": str},
                "underlyingSecurityShares": {"tags": "underlyingSecurity", "encoding": _int_or_float},
                "underlyingSecurityValue": {"tags": "underlyingSecurity", "encoding": float},
            },
            "nonDerivativeTransaction": {
                "securityTitle": {"footnotes": True},
                "transactionDate": {"footnotes": True, "encoding": _xs_date},
                "deemedExecutionDate": {"encoding": _xs_date, "footnotes": True},
                "transactionFormType": {"tags": "transactionCoding", "footnotes": False, "encoding": str},
                "transactionCode": {"tags": "transactionCoding", "footnotes": False, "encoding": str},
                "equitySwapInvolved": {"tags": "transactionCoding", "footnotes": False, "encoding": _bool_from_string},
                "transactionTimeliness": {},
                "transactionShares": {"tags": "transactionAmounts", "encoding": _int_or_float},
                "transactionPricePerShare": {"tags": "transactionAmounts", "encoding": float},
                "transactionAcquiredDisposedCode": {"tags": "transactionAmounts"},
                "sharesOwnedFollowingTransaction": {"tags": "postTransactionAmounts", "encoding": _int_or_float},
                "valueOwnedFollowingTransaction": {"tags": "postTransactionAmounts", "encoding": float},
                "directOrIndirectOwnership": {"tags": "ownershipNature"},
                "natureOfOwnership": {"tags": "ownershipNature"},
            }
        }

        self.transactions = {
            "nonDerivativeTransaction": [],
            "nonDerivativeHolding": [],
            "derivativeTransaction": [],
            "derivativeHolding": []
        }

        old_transaction_types = {'nonDerivativeSecurity': 'nonDerivativeTransaction',
                                 'derivativeSecurity': 'derivativeTransaction'}

        for transaction in self.tree.findall('nonDerivativeTable/*'):
            tag = transaction.tag
            self.transactions[tag].append(self._get_entries(transaction_instructions[tag], transaction))

        for transaction in self.tree.findall('derivativeTable/*'):
            tag = transaction.tag
            self.transactions[tag].append(self._get_entries(transaction_instructions[tag], transaction))

        for oldtag in old_transaction_types:
            for transaction in self.tree.findall(oldtag):
                tag = transaction.tag
                self.transactions[old_transaction_types[oldtag]].append(self._get_entries(transaction_instructions[old_transaction_types[oldtag]], transaction))

    def _get_entries(self, instructions, tree):
        output = {}
        if tree is not None:
            for tag, instructions in instructions.iteritems():
                try:
                    tags = instructions['tags']
                    if isinstance(tags, str):
                        tags = [tags] + [tag]
                    else:
                        tags = tags + [tag]
                except KeyError:
                    tags = [tag]

                footnotes = instructions.get('footnotes', True)
                encoding = instructions.get('encoding', str)

                output[tag] = Entry(tree, tags, footnotes, encoding)
        else:
            return None
        return output

    def get_transactions(self):
        output = []
        for key, value in self.transactions.iteritems():
            current_transactions = []
            for transaction in value:
                current_transaction = {'type':key}
                current_footnotes = set()
                for field_name, field_value in transaction.iteritems():
                    current_transaction[field_name] = field_value.value
                    if field_value.has_footnotes:
                        current_footnotes |= set(field_value.footnotes) # union set operator

                footnotes_string = []
                for footnote in current_footnotes:
                    footnotes_string.append(self.footnotes[footnote])
                current_transaction['footnotes'] = "; ".join(footnotes_string)
                current_transactions.append(current_transaction)
            output += current_transactions

        return output

    def to_dict(self):
        output = {}
        output['otherdata'] = self.otherdata
        output['ownerSignature'] = self.ownerSignature
        output['reportingOwner'] = self.reportingOwner
        output['issuer'] = self.issuer
        output['transactions'] = self.get_transactions()
        return output

    def to_html(self):

        template = Environment(loader=FileSystemLoader('')).get_template('form4_template.html')

        def print_with_footnote(item):
            if isinstance(item.value, datetime.datetime):
                output = item.value.strftime("%Y-%m-%d")
            else:
                output = str(item.value) if item is not None and item.value is not None else ""
            if item.has_footnotes:
                for footnote in item.footnotes:
                    output += '</span><span class="FootnoteData"><sup>({})</sup>'.format(footnote.replace('F', ''))
            return output

        def process_sig(item):
            if isinstance(item, datetime.datetime):
                output = item.strftime("%Y-%m-%d")
            else:
                output = str(item).replace('/s/','') if item is not None else ""
            return output

        fnoutput = [str(k)+'. '+self.footnotes['F'+str(k)] for k in sorted([int(x[1:]) for x in self.footnotes])]

        return template.render(form4=self, process=print_with_footnote, sig=process_sig,
                               footnoteoutput=fnoutput).encode('utf-8')


class Entry(object):
    def __init__(self, tree, route, footnotes=True, encoding=str):

        tree = self.get_item(tree, route)
        self.has_footnotes = footnotes

        if encoding is None:
            encoding = lambda v: v

        if footnotes:
            try:
                self.value = encoding(tree.value)
            except AttributeError:
                self.value = None

            try:
                self.footnotes = [x.attrib['id'] for x in tree.footnoteId]
            except AttributeError:
                self.footnotes = []

        else:
            self.footnotes = None
            try:
                self.value = encoding(tree)
            except AttributeError:
                self.value = None

    def __repr__(self):
        if self.has_footnotes:
            return str(self.value) + " (fn: " + ",".join(self.footnotes) + ")"
        return str(self.value)

    def get_item(self, tree, route):
        current_tree = tree
        for item in route:
            try:
                current_tree = current_tree[item]
            except AttributeError as a:
                return None

        return current_tree



