import datetime
import re
import json
import uu
import cStringIO
import warnings
import os

_meta_tag_pattern = re.compile(r'^<([A-Z _-].+?)>(.*)$', re.MULTILINE)
_colon_tag_pattern = re.compile(r'^(.+?):(.*)$', re.MULTILINE)
_uuencoded_pattern = re.compile(r'begin 644 (.+\.\w+)\n(?:(?:.+\n)+)end')
_sic_pattern = re.compile(r'\[(\d+)\]')
_item_pattern = re.compile(r'<ITEM-NUMBER>(.+)\n<ITEM-PERIOD>(.+)\n</ITEM>')


class DateTimeEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime.datetime):
            return o.isoformat()
        return json.JSONEncoder.default(self, o)


def _process_line(line):
    return line.upper().replace("\t", "")


def process_date(string):
    try:
        return datetime.datetime.strptime(string.strip(), '%Y%m%d')
    except ValueError:
        return string


def process_date_time(string):
    try:
        return datetime.datetime.strptime(string.strip(), '%Y%m%d%H%M%S')
    except ValueError:
        return string


class SecFiling(object):
    """
    Args:
        filename (str): The filename of the form
        contents (str): The raw text dump of the form
    """

    _company_types = {
            'SERIAL COMPANY:': {
                'type': 'SERIAL COMPANY',
                'suffixes': '</SERIAL-COMPANY>'
            },
            '<SERIAL-COMPANY>': {
                'type': 'SERIAL COMPANY',
                'suffixes': '</SERIAL-COMPANY>'
            },
            'FILER:': {
                'type': 'FILER',
                'suffixes': '</FILER>'
            },
            'FILED BY:': {
                'type': 'FILED BY',
                'suffixes': '</FILED-BY>'
            },
            '<FILER>': {
                'type': 'FILER',
                'suffixes': '</FILER>'
            },
            'FILED FOR:': {
                'type': 'FILED FOR',
                'suffixes': '</FILER>'
            },
            'FILED-FOR:': {
                'type': 'FILED FOR',
                'suffixes': '</FILER>'
            },
            '<FILED-FOR>': {
                'type': 'FILED FOR',
                'suffixes': '</FILED-FOR>'
            },
            'REPORTING OWNER:': {
                'type': 'REPORTING OWNER',
                'suffixes': '</REPORTING-OWNER>'
            },
            'REPORTING-OWNER:': {
                'type': 'REPORTING OWNER',
                'suffixes': '</REPORTING-OWNER>'
            },
            '<REPORTING-OWNER>': {
                'type': 'REPORTING OWNER',
                'suffixes': '</REPORTING-OWNER>'
            },
            'ISSUER:': {
                'type': 'ISSUER',
                'suffixes': '</ISSUER>'
            },
            '<ISSUER>': {
                'type': 'ISSUER',
                'suffixes': '</ISSUER>'
            },
            'SUBJECT COMPANY:': {
                'type': 'SUBJECT COMPANY',
                'suffixes': '</SUBJECT-COMPANY>'
            }
        }

    def __init__(self, filename, contents):
        self.filename = filename
        """This is the Filename of the form provided. Usually in .txt format"""
        """List of SecDocument objects. Each one corresponding to a document attatched to the form"""
        """Date of filing, extracted from the header of the form object"""
        self.companies = []
        """List of SecCompany objects. Each one corresponding to a company associated with the form. See SecCompany."""
        self.metadata = {}
        self.documents = []
        """Miscellaneous meta data attatched to the form in a dictionary format."""
        self._extract_header_info(contents)
        self._convert_metadata_types()
        self._extract_documents(contents)

        ## Usually get date and type from header, but if header is missing...
        if 'FILED AS OF DATE' in self.metadata:
            self.date = self.metadata['FILED AS OF DATE']
        else:
            self.date = None
        if 'CONFORMED SUBMISSION TYPE' in self.metadata:
            self.type = self.metadata['CONFORMED SUBMISSION TYPE']
        elif len(self.documents) > 0 and 'TYPE' in self.documents[0].metadata:
            self.type = self.documents[0].metadata['TYPE']
        else:
            self.type = None


    def __repr__(self):
        header = self.metadata['SEC-HEADER'] if 'SEC-HEADER' in self.metadata else self.metadata['IMS-HEADER']
        return 'SecFiling: filename=' + self.filename + '; header=' + header

    def to_dict(self, include_text=False):
        form = {
            'DATE': self.date,
            'FILENAME': self.filename,
            'TYPE': self.type
        }

        if len(self.metadata) > 0:
            form['METADATA'] = self.metadata

        if len(self.companies) > 0:
            for company in self.companies:
                if company.type not in form:
                    form[company.type] = [company.to_dict()]
                else:
                    form[company.type].append(company.to_dict())

        if len(self.documents) > 0:
            form['DOCUMENTS'] = [document.to_dict(include_text) for document in self.documents]

        return form

    # def to_row(self):
    #     return [self.date, self.filename, self.type]
    #
    #
    # def row_names(self):
    #     return ['DATE', 'FILENAME', 'FORM TYPE']

    def to_json(self, path="", filename=None, save_file=True, include_text=False, output=False):
        name = (self.filename.split('/')[-1].split('.')[0] + '.json') if filename is None else filename
        path_to_save = os.path.join(path, name)

        file_output = json.dumps(self.to_dict(include_text), sort_keys=False, indent=4, separators=(',', ': '), cls= DateTimeEncoder)

        if save_file:
            with open(path_to_save, 'w+') as output_file:
                output_file.write(file_output)

        if output:
            return json.loads(file_output)

    def _extract_documents(self, contents):
        lines = contents.splitlines()
        idx = 0

        def sw(line, tags):
            return any(line.lstrip().startswith(t) for t in tags)

        while idx < len(lines):
            while idx < len(lines) and not sw(lines[idx], ["<DOCUMENT>"]):
                idx += 1

            if idx >= len(lines): ## end of document, could be reached because of extra lines after last doc
                break
            current_document = []
            current_header = []
            while not sw(lines[idx], ["<TEXT>"]):
                current_header.append(lines[idx].strip())
                idx += 1
            idx += 1
            while not sw(lines[idx], ["</TEXT>", "</DOCUMENT>", "</SEC-DOCUMENT>", "<DOCUMENT>"]):
                current_document.append(lines[idx])
                idx += 1

            full_header, full_doc = '\n'.join(current_header), '\n'.join(current_document)
            self.documents.append(SecDocument(full_header, full_doc))


    def _process_rule(self, lines, idx):
        self.metadata['RULE'] = {}
        idx += 1
        if lines[idx].startswith('<RULE-NAME>'):
            self.metadata['RULE']['RULE-NAME'] = lines[idx][11:].strip()
        else:
            raise Exception('gsbsec.SecFiling: malformed rule in header')
        idx += 1
        self.metadata['RULE']['ITEMS'] = []
        while lines[idx].startswith('<ITEM>'):
            idx += 1
            m = _item_pattern.match('\n'.join(lines[idx:idx+3]))
            if m is not None:
                self.metadata['RULE']['ITEMS'].append({'ITEM-NUMBER':m.group(1), 'ITEM-PERIOD':process_date(m.group(2))})
            else:
                raise Exception('gsbsec.SecFiling: malformed rule item in header')
            idx += 3
        if lines[idx].startswith('</RULE>'):
            idx += 1
        else:
            raise Exception('gsbsec.SecFiling: malformed rule in header')
        return idx

    def _extract_header_info(self, contents):

        lines = [l.replace('\t', '') for l in contents.splitlines()]
        idx = 0

        while idx < len(lines) and not lines[idx].startswith(('<SEC-DOCUMENT>', '<IMS-DOCUMENT>')):
            idx += 1

        if idx == len(lines):
            warnings.warn('gsbsec.SecFiling: No Header Information in file '+self.filename)
            return

        if lines[idx].startswith('<SEC-DOCUMENT>'):
            self.metadata['SEC-DOCUMENT'] = lines[idx].replace('<SEC-DOCUMENT>', '')
        elif lines[idx].startswith('<IMS-DOCUMENT>'):
            self.metadata['IMS-DOCUMENT'] = lines[idx].replace('<IMS-DOCUMENT>', '')

        idx += 1
        if not lines[idx].startswith(('<SEC-HEADER>', '<IMS-HEADER>')):
            if 'IMS-DOCUMENT' in self.metadata:
                if '<IMS-HEADER>' in self.metadata['IMS-DOCUMENT']:# missing line break
                    pieces = self.metadata['IMS-DOCUMENT'].split('<IMS-HEADER>')
                    self.metadata['IMS-DOCUMENT'] = pieces[0]
                    self.metadata['IMS-HEADER'] = pieces[1]
            elif 'SEC-DOCUMENT' in self.metadata:
                if '<SEC-HEADER>' in self.metadata['SEC-DOCUMENT']:
                    pieces = self.metadata['SEC-DOCUMENT'].split('<SEC-HEADER')
                    self.metadata['SEC-DOCUMENT'] = pieces[0]
                    self.metadata['SEC-HEADER'] = pieces[1]
        elif lines[idx].startswith('<SEC-HEADER>'):
            self.metadata['SEC-HEADER'] = lines[idx].replace('<SEC-HEADER>', '')
        elif lines[idx].startswith('<IMS-HEADER>'):
            self.metadata['IMS-HEADER'] = lines[idx].replace('<IMS-HEADER>', '')

        filer_prefixes = tuple([k for k in self._company_types])

        while not lines[idx].startswith(filer_prefixes):
            m, c = _meta_tag_pattern.match(lines[idx]), _colon_tag_pattern.match(lines[idx])
            if m is not None:
                tag_name = _process_line(m.group(1))
                if tag_name != 'RULE':
                    self.metadata[tag_name] = m.group(2)
                else:
                    idx = self._process_rule(lines, idx)
            elif c is not None:
                tag_name = _process_line(c.group(1))
                self.metadata[tag_name] = c.group(2)
            elif len(lines[idx].strip()) > 0:
                raise Exception('gsbsec.SecFiling: unexpected header information: '+lines[idx][:200])
            idx += 1

        while idx < len(lines):
            if len(lines[idx]) == 0:
                idx += 1
            elif lines[idx].startswith(filer_prefixes):
                for filer_tag, filer_type in self._company_types.iteritems():
                    if lines[idx].startswith(filer_tag):
                        chunk = []
                        idx += 1
                        current_escape = ('</SEC-HEADER>', '</IMS-HEADER>', filer_type['suffixes'])
                        not_tuple = current_escape + filer_prefixes
                        while not lines[idx].startswith(not_tuple):
                            if len(lines[idx]) > 0:
                                chunk.append(lines[idx])
                            idx += 1
                            if lines[idx].startswith(filer_type['suffixes']):
                                idx += 1
                                break
                        self.companies.append(SecCompany(chunk, filer_type['type']))
                        break
            elif lines[idx].startswith(('</SEC-HEADER>', '</IMS-HEADER>')):
                break
            else:
                raise Exception('GSBSEC.SecFiling: Encountered unexpected header info: ' + lines[idx])


    def _convert_metadata_types(self):
        key_functions = {
            "DATE AS OF CHANGE": process_date,
            "PUBLIC DOCUMENT COUNT": int,
            "ACCEPTANCE DATETIME":  process_date_time,
            "FILED AS OF DATE": process_date,
            "CONFORMED PERIOD OF REPORT": process_date
        }
        for k in self.metadata:
            if k in key_functions:
                self.metadata[k] = key_functions[k](self.metadata[k])
            elif 'DATETIME' in k:
                self.metadata[k] = process_date_time(self.metadata[k])
            elif 'DATE' in k:
                self.metadata[k] = process_date(self.metadata[k])

    def get_reporting_owners(self):
        return [company for company in self.companies if company.type == "REPORTING OWNER"]


class SecDocument(object):

    def __init__(self, header, text):
        self.header = header
        self.text = text
        """Pure stripped text content of the document if necessary"""
        self.metadata = {}
        """Dictionary of all miscellaneous metadata of document in dictionary format"""
        self._extract_meta(header)

    def __repr__(self):
        if 'FILENAME' in self.metadata:
            return 'SecDocument: ' + self.metadata['FILENAME']
        elif 'SEQUENCE' in self.metadata:
            return 'SecDocument: ' + str(self.metadata['SEQUENCE'])
        else:
            return 'SecDocument - Unknown Filename'

    def to_dict(self, include_text=False):
        result = self.metadata
        if include_text:
            result['TEXT'] = self.text
        return result


    def to_file(self, path="", filename=None, save_file=True):
        # default filenames
        if filename is None:
            if 'FILENAME' in self.metadata:
                filename = self.metadata['FILENAME']
            else:
                filename = 'DOCUMENT-'+str(self.metadata['SEQUENCE'])

        uuencoded_file = _uuencoded_pattern.search(self.text)
        path_to_save = os.path.join(path, filename)
        file_output = self.uu2string(uuencoded_file.group(0)) if uuencoded_file else self.text

        if file_output.startswith('<XML>'):
            file_output = "\n".join(file_output.split("\n")[1:-2])

        if save_file:
            with open(path_to_save, 'w+') as output_file:
                output_file.write(file_output)

        return file_output

    def _extract_meta(self, contents):
        """Assumes that each meta tag for a document appears only once
        """
        for line in contents.splitlines():
            if line.startswith('<TEXT>'):
                break  # done with header info
            m = _meta_tag_pattern.match(line)
            if m:
                tag = m.group(1).upper().replace("\t", "")
                value = m.group(2)
                if tag == 'SEQUENCE':
                    self.metadata[tag] = int(value)
                else:
                    self.metadata[tag] = value

    def uu2string(self, mode=None):
        outfile = cStringIO.StringIO()
        infile = cStringIO.StringIO(self.text)
        uu.decode(infile, outfile, mode, quiet=True)
        return outfile.getvalue()


class SecCompany(object):

    """
    Object containing details on an associated company. Members are shown below.
    """
    def __init__(self, line_data, company_type):
        self.type = company_type
        """Company type. SERIAL COMPANY, FILER, FILED BY, FILED FOR, REPORTING OWNER, ISSUER, SUBJECT COMPANY"""
        self.metadata = {}
        """Company metadata provided in dictionary format"""
        self.filing_values = {}
        """Dictionary containing filing values"""
        self.addresses = []
        """List of addresses (usually business or mailing)"""
        self.former_companies = []
        """List of former companies associated with filing"""
        self._processlinedata(line_data)
        self.sic = self.get_sic()
        self.cik = self.get_cik()


    def __repr__(self):
        return 'SecCompany: ' + self.type + ': ' + self.metadata['COMPANY CONFORMED NAME']

    def get_sic(self):
            if 'STANDARD INDUSTRIAL CLASSIFICATION' in self.metadata:
                try:
                    output = int(self.metadata['STANDARD INDUSTRIAL CLASSIFICATION'])
                except ValueError:
                    try:
                        output = int(_sic_pattern.search(self.metadata['STANDARD INDUSTRIAL CLASSIFICATION']).group(1))
                    except AttributeError:
                        output = None
            else:
                output = None

            return output

    def get_cik(self):
            if 'CENTRAL INDEX KEY' in self.metadata:
                try:
                    output = str(self.metadata['CENTRAL INDEX KEY'])
                except ValueError:
                    output = None
            else:
                output = None

            return output

    def to_dict(self):
        filer = {}

        if len(self.metadata) > 0:
            filer['COMPANY DATA'] = self.metadata

        if len(self.filing_values) > 0:
            filer['FILING VALUES'] = self.filing_values

        filer['ADDRESSES'] = {address.type: address.fields for address in self.addresses}

        if self.sic is not None:
            filer['SIC'] = self.sic


        if self.cik is not None:
            filer['CIK'] = self.cik


        if len(self.former_companies) > 0:
            filer['FORMER COMPANIES'] = []
            for a in self.former_companies:
                filer['FORMER COMPANIES'].append(a)

        return filer

    def _processlinedata(self, lines):
        tag_dict = {
            'COMPANY DATA:': {
                'object': self.metadata,
                'suffixes': '</COMPANY-DATA>'
            },
            'OWNER DATA:': {
                'object': self.metadata,
                'suffixes': '</OWNER-DATA>',
            },
            'FILING VALUES:': {
                'object': self.filing_values,
                'suffixes': '</FILING-VALUES>'
            },
            'FORMER COMPANY:': {
                'object': self.former_companies,
                'suffixes': '</FORMER-COMPANY>'
            },
            'FORMER NAME:': {
                'object': self.former_companies,
                'suffixes': '</FORMER-NAME>'
            },
            'BUSINESS ADDRESS:': {
                'object': self.addresses,
                'suffixes': '</BUSINESS-ADDRESS>',
                'address-class': 'BUSINESS',
            },
            'MAIL ADDRESS:': {
                'object': self.addresses,
                'suffixes': '</MAIL-ADDRESS>',
                'address-class': 'MAIL'
            }
        }

        line_tags = tuple([k for k in tag_dict])

        idx = 0

        chunk = []
        while idx < len(lines):
            if lines[idx].startswith(line_tags):
                for tag, tag_object in tag_dict.iteritems():
                    if lines[idx].startswith(tag):
                        chunk = []
                        idx += 1
                        current_escape = ('</SEC-HEADER>', tag_object['suffixes'])
                        not_tuple = current_escape + line_tags
                        while idx < len(lines) and not lines[idx].startswith(not_tuple):
                            if len(lines[idx]) > 0:
                                chunk.append(lines[idx])
                            idx += 1
                        if tag_object['object'] is self.addresses:
                            new_address = Address(tag_object['address-class'])
                            self._chunk_to_dict(chunk, new_address.fields)
                            self.addresses.append(new_address)
                        elif tag_object['object'] is self.former_companies:
                            new_former_company = {}
                            self._chunk_to_dict(chunk, new_former_company)
                            self.former_companies.append(new_former_company)
                        else:
                            self._chunk_to_dict(chunk, tag_object['object'])
                        if idx < len(lines) and lines[idx].startswith(current_escape):
                            idx += 1
                        break
            elif len(lines[idx]) == 0:
                idx += 1
            else: ## unexpected tag to start the line
                raise Exception("GSBSEC.SecCompany: Unexpected Filer/company info: " + lines[idx])


    @staticmethod
    def _chunk_to_dict(line_data, storagedict):

        for line in line_data:
            info = line.strip().split(':', 1)
            if len(info) == 2:
                tag = _process_line(info[0])
                if "CONFORMED" in tag:
                    #key = re.sub(r'/([A-Z]){2}/*', "", info[1]).strip()
                    key = info[1].strip()
                elif "DATE" in tag:
                    try:
                        key = datetime.datetime.strptime(info[1].strip(), '%Y%m%d').date().strftime('%Y-%m-%d')
                    except TypeError:
                        key = info[1].strip()
                else:
                    key = info[1].strip()

                storagedict[tag] = key


class Address(object):
    common_fields = ['STREET 1', 'STREET 2', 'CITY', 'STATE', 'ZIP']

    def __init__(self, address_type):
        self.type = address_type
        self.fields = {}

    def __repr__(self):
        return self.type + ' ADDRESS'

    def pretty(self):
        result = []
        justsize = max([len(k)+1 for k in self.fields]+[12])
        for field in self.common_fields:
            if field in self.fields:
                result.append(field.ljust(justsize) + self.fields[field])
        for field in [x for x in self.fields if x not in self.common_fields]:
            result.append(field.ljust(justsize) + self.fields[field])

        return "\n".join(result)
