'''
Testing script to run on a yen.  Should complete successfully without raising any exceptions.

'''

import gzip
import os
import random
import sys

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))

from form4 import Form4
from utilities import *

from secdoc import SecFiling


## Doesn't allow Form4 because there are too many filings and it takes forever; if you want Form4, use fxn below
def get_random_file_contents():
    try:
        dir = '/ifs/gsb/EDGAR/'
        dir = os.path.join(dir, random.choice(os.listdir(dir))) #form type
        while dir in ['/ifs/gsb/EDGAR/idx_files', '/ifs/gsb/EDGAR/Form4'] or dir.startswith('.'):
            dir = '/ifs/gsb/EDGAR/'
            dir = os.path.join(dir, random.choice(os.listdir(dir))) #form type
        dir = os.path.join(dir, random.choice(os.listdir(dir))) #year
        randomfile = os.path.join(dir, random.choice(os.listdir(dir))) #file
    except IndexError:
        return get_random_file_contents()
    with gzip.open(randomfile, 'r') as filingfile:
        return randomfile, filingfile.read()


## You don't want to use this with Form4, but it's not prohibited; use fxn below instead
def get_random_file_contents_type(type):
    try:
        dir = '/ifs/gsb/EDGAR/{}'.format(type)
        dir = os.path.join(dir, random.choice(os.listdir(dir))) #year
        randomfile = os.path.join(dir, random.choice(os.listdir(dir))) #file
    except IndexError:
        return get_random_file_contents()
    with gzip.open(randomfile, 'r') as filingfile:
        return randomfile, filingfile.read()


form4files = []

## creating the index takes a long time to run
def create_form4_index():
    dir = '/ifs/gsb/EDGAR/Form4'
    with open('form4index.txt', 'w') as of:
        for yeardir in os.listdir('/ifs/gsb/EDGAR/Form4'):
            print 'creating index for {}'.format(yeardir)
            for filename in os.listdir(os.path.join(dir, yeardir)):
                if filename.endswith('.gz'):
                    fname = os.path.join(dir, yeardir, filename)
                    of.write(fname+'\n')
                    form4files.append(fname)


## relies on text file with all files listed; you need to update the index file with create_form4_index()
## if you want newer Form 4s to be included
def get_random_form4():
    global form4files
    if form4files is None or len(form4files) == 0:
        with open('form4index.txt', 'r') as df:
            form4files = [x.strip() for x in df.readlines()]
    fname = random.choice(form4files)
    with gzip.open(fname, 'r') as filingfile:
        return fname, filingfile.read()


'''
Process some random files and make sure we don't get an exception
'''

def check_random_files():
    for i in range(100):
        filename, contents = get_random_file_contents()
        print 'Test Document {}/100'.format(str(i+1))
        print filename
        filing = SecFiling(filename, contents)
        if 'PUBLIC DOCUMENT COUNT' in filing.metadata:
            if int(filing.metadata['PUBLIC DOCUMENT COUNT']) != len(filing.documents):
                print '\t!!!Mismatch between header doc count and number of extracted documents!!!'
        for doc in filing.documents:
            if is_html(doc.text):
                remove_external_content(doc.text)


def get_filing(filename):
    with gzip.open(filename, 'r') as df:
        return SecFiling(filename, df.read())


'''
Process some known files and make sure they conform to known values
'''
def check_known_files():
    print '\nChecking early IMS-Document filing'
    filing = get_filing('/archive/gsb/EDGAR/Form10-K/1993/861439_4_0000912057-94-000263.txt.gz')
    print filing.metadata

    print '\nPrivacy header, company data closing tags'
    filing = get_filing('/archive/gsb/EDGAR/FormU-9C-3_a/2000/754737_3_0000754737-00-000017.txt.gz')
    print filing.metadata

    print '\nBad line break in company data; errant ]'
    filing = get_filing('/archive/gsb/EDGAR/Form424B1/2000/1109138_3_0000912057-00-033682.txt.gz')
    if len(filing.companies) != 1:
        raise Exception('Wrong number of companies')
    if filing.companies[0].type != 'FILER':
        raise Exception('Wrong company type')
    if len(filing.companies[0].addresses) != 2:
        raise Exception('Wrong number of company addresses')
    print filing.companies[0].metadata

    print '\nNo Header, Just a Document: should get a non-fatal warning'
    filing = get_filing('/archive/gsb/EDGAR/FormDFAN14A/1995/37008_1_0000950136-95-000113.txt.gz')
    print 'Filing type (derived from document, should be DFAN14A): {}'.format(filing.type)

    print '\n<PAPER> in header'
    filing = get_filing('/archive/gsb/EDGAR/FormWDL-REQ/2005/1336336_4_9999999997-05-042627.txt.gz')
    print filing.metadata

    print '\n<IMS-HEADER> opening tag not on a new line'
    filing = get_filing('/archive/gsb/EDGAR/Form10-C_a/1995/42119_2_0000929859-95-000016.txt.gz')
    print filing.metadata

    print '\nUncommon header field, with no value after colon; should have CONFIRMING COPY in metadata'
    filing = get_filing('/archive/gsb/EDGAR/Form485A24F/1996/902042_2_0000902042-96-000036.txt.gz')
    print filing.metadata
    if 'CONFIRMING COPY' not in filing.metadata:
        raise Exception('Missed a value in header')

    print '\n<RULE></RULE> section in header (2 files)'
    filing = get_filing('/archive/gsb/EDGAR/FormSD_a/2015/1105472_2_0001104659-15-046474.txt.gz')
    print filing.metadata
    filing = get_filing('/archive/gsb/EDGAR/FormSD/2014/9984_2_0001193125-14-221750.txt.gz')
    print filing.metadata

    print '\nIndented </TEXT> tag'
    filing = get_filing('/archive/gsb/EDGAR/Form8-B12B_a/1994/831300_1_0000950005-94-000002.txt.gz')
    print filing.documents
    if len(filing.documents) != 2:
        raise Exception('Missed extracting the document')

    print '\nMalformed closing text tag for document'
    filing = get_filing('/archive/gsb/EDGAR/Form10-K/1996/96271_1_0000096271-96-000001.txt.gz')
    print filing.documents
    if len(filing.documents) != 7:
        raise Exception('Wrong number of documents')

    print '\n</DOCUMENT> tag on same line with </TEXT>'
    filing = get_filing('/archive/gsb/EDGAR/Form10-K/1998/852953_2_0000810663-98-000029.txt.gz')
    print filing.documents
    if len(filing.documents) != 2:
        raise Exception('Wrong number of documents')
    if not filing.documents[1].text.endswith('</TABLE>'):
        raise Exception('Missed end of document')

    print '\nMissing </DOCUMENT> and </TEXT> for final document'
    filing = get_filing('/archive/gsb/EDGAR/Form10-K/1999/889604_1_0000889604-99-000004.txt.gz')
    print filing.documents
    if not filing.documents[-1].text.endswith('</TABLE>'):
        raise Exception('Wrong end of document')


##################
## Test Form4
##################


def ensure_all_transactions(secdocument, form4):
    '''
    Ensures the correct amount of transactions in each form
    '''
    text = secdocument.text
    types = ['derivativeHolding', 'derivativeTransaction', 'nonDerivativeTransaction', 'nonDerivativeHolding']
    for type in types:
        if len(re.compile(r'<' + re.escape(type) + r'>').findall(text)) > len(form4.transactions[type]):
            print text
            raise Exception("Not captured all " + type)




def check_form4():
    print "\nRunning Form 4 tests..."

    form4tests(True)
    form4tests()


def form4tests(form4a=False):
    for i in range(100):
        if form4a:
            filename, contents = get_random_file_contents_type('Form4_a')
        else:
            filename, contents = get_random_form4()
        print 'Test Form4{} {}/100: {}'.format('/A' if form4a else '', str(i + 1), filename)
        filing = get_filing(filename)
        for document in filing.documents:
            if ((form4a and document.metadata['TYPE'] == '4/A') or
                    (not form4a and document.metadata['TYPE'] == "4")):
                if is_xml(document.text):
                    try:
                        form4 = Form4(document.text)
                        print "Made Form4 for {}, schema version {} ".format(str(document), form4.otherdata["schemaVersion"])
                        for key, val in form4.transactions.items():
                            if len(val) > 0:
                                print key
                        if 'nonDerivativeSecurity' in document.text:
                            print '*** nonDerivativeSecurity ***'
                        if 'derivativeSecurity' in document.text:
                            print '*** derivativeSecurity ***'
                        if len(form4.reportingOwner) > 1:
                            print '!!!!!!! More than 1 reporting owner !!!!!!!!!'
                            if (form4.reportingOwner[0]["isOfficer"].value != form4.reportingOwner[1]["isOfficer"].value or
                                form4.reportingOwner[0]["isDirector"].value != form4.reportingOwner[1]["isDirector"].value or
                                form4.reportingOwner[0]["isTenPercentOwner"].value != form4.reportingOwner[1]["isTenPercentOwner"].value or
                                form4.reportingOwner[0]["isOther"].value != form4.reportingOwner[1]["isOther"].value):
                                raise Exception('Owner info mismatch')
                    except Exception as e:
                        if e.message.startswith('Incompatible schemaVersion'):
                            print 'Old schema correctly caught'
                        else:
                            print e.message
                            #traceback.print_exc()
                            print 'Could not make Form4 object'
                            #print document.text[:100]
                    else:
                        ensure_all_transactions(document, form4)
                else:
                    print 'Form is not XML'
                break
        else:
            print 'No Form 4{} object in filing'.format('/A' if form4a else '')
            for doc in filing.documents:
                if ((form4a and document.metadata['TYPE'] == '4/A') or
                    (not form4a and document.metadata['TYPE'] == "4")):
                    print str(document)


def check_form4_known():
    print 'Form 4 known checks\n\n'
    print 'testing file with old schema'
    filing = get_filing('/ifs/gsb/EDGAR/Form4_a/2003/1215185_2_0000740260-03-000084.txt.gz') ## bad tag value (old schema)
    for doc in filing.documents:
        if doc.metadata['TYPE'] == '4/A':
            try:
                form4 = Form4(doc.text)
            except Exception as e:
                print "Got an exception as expected"
            else:
                raise Exception("Should have got an exception when made Form4")

    print '\nOld schema X0202'
    filing = get_filing('/ifs/gsb/EDGAR/Form4_a/2006/794367_2_0000794367-06-000098.txt.gz')
    for doc in filing.documents:
        if doc.metadata['TYPE'] == '4/A':
            form4 = Form4(doc.text)
            html = form4.to_html()
            ensure_all_transactions(doc, form4)
            print 'Looking up value: {}'.format(form4.transactions['derivativeTransaction'][0]['transactionShares'].value)
            print form4.transactions['derivativeTransaction'][0]['transactionShares'].value == 9137
            print 'Above should be true'

    print '\nBad date format'
    filing = get_filing('/ifs/gsb/EDGAR/Form4/2013/886982_3_0000769993-13-000450.txt.gz')
    for doc in filing.documents:
        if doc.metadata['TYPE'] == '4':
            form4 = Form4(doc.text)
            print form4.ownerSignature


def html_and_transactions(filename):
    print '\n'
    print filename
    filing = get_filing(filename)
    print original_url(filename)
    for doc in filing.documents:
        if doc.metadata['TYPE'] == '4' or doc.metadata['TYPE'] == '4/A':
            form4 = Form4(doc.text)
            with open('/ifs/gsb/cmaimone/gsbsec/genhtml/{}.html'.format(core_filename(filename)), 'w') as of:
                of.write(form4.to_html())
            print form4.to_dict()
            print 'Transactions:'
            for key, item in form4.transactions.items():
                print key
                for t in item:
                    print t
            return form4
    print 'no Form4 created!!!'
    return None


def form4_analysis():
    html_and_transactions('/ifs/gsb/EDGAR/Form4_a/2006/794367_2_0000794367-06-000098.txt.gz')
    f1 = html_and_transactions('/ifs/gsb/EDGAR/Form4/2007/1399289_4_0001225208-07-011073.txt.gz')
    html_and_transactions('/ifs/gsb/EDGAR/Form4/2012/916863_1_0000916863-12-000083.txt.gz')

    print '\nderivativeSecurity (and non) Items\n-----------------------'
    f1 = html_and_transactions('/ifs/gsb/EDGAR/Form4_a/2003/930803_3_0001253065-03-000002.txt.gz')
    if f1.reportingOwner[0]['isOfficer'].value:
        raise Exception("Should not be an officer!")
    html_and_transactions('/ifs/gsb/EDGAR/Form4_a/2003/1228443_2_0001228443-03-000005.txt.gz')

    html_and_transactions('/ifs/gsb/EDGAR/Form4/2003/1001082_2_0001110592-03-000002.txt.gz')

    print '\nX0202 Items\n------------------------'
    html_and_transactions('/ifs/gsb/EDGAR/Form4/2004/1204766_3_0000813920-04-000049.txt.gz')

    print '\nMultiple Reporting Owners'
    html_and_transactions('/ifs/gsb/EDGAR/Form4/2006/1245558_3_0001365133-06-000005.txt.gz')
    html_and_transactions('/ifs/gsb/EDGAR/Form4/2013/1511737_3_0001179110-13-013963.txt.gz')


def check_documentation_example():
    f1 = html_and_transactions('/ifs/gsb/EDGAR/Form4/2007/1399289_4_0001225208-07-011073.txt.gz')
    print '******************'
    print f1.transactions
    print '-----------------'
    for transactiontype, typetransactions in f1.transactions.items():
        print transactiontype
        for trans in typetransactions:
            for key, entry in trans.items():
                print "  {}: {}, {}".format(key, entry.value, entry.footnotes)
                if entry.footnotes is not None:
                    for fn in entry.footnotes:
                        print '   {}: {}'.format(fn, f1.footnotes[fn])


if __name__ == '__main__':
    #create_form4_index() ## need to make an index file first if you're going to use check_form4; takes a bit to run

    #check_form4()

    check_random_files()

    #form4_analysis()

    #check_form4_known()








