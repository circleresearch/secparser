
import re
from bs4 import BeautifulSoup, NavigableString



###########################################
# quick formatting
##########################################


def pad_cik(cik):
    return '0'*(10-len(str(cik)))+str(cik)


## get SEC url from version of filename of files downloaded to yens
def original_url(filename):
    fname = core_filename(filename)
    return 'https://www.sec.gov/Archives/edgar/data/{}/{}/{}-index.htm'.format(fname.split('_')[0],
                                                                               fname.split('_')[-1].replace('-', ''),
                                                                               fname.split('_')[-1])

def core_filename(filename):
    fname = filename.split('/')[-1]
    fname = fname.split('.')[0]
    return fname

#########################################
# Detect XML for Form 4
#########################################

def is_xml(text):
    tu = text.upper().strip()
    return tu.startswith('<XML') or tu.startswith('<?XML')


##########################################
# text processing
##########################################

map1252 = {128: u"\u20AC", 130: u"\u201A", 131: u"\u0192", 132: u"\u201E",
            133: u"\u2026", 134: u"\u2020", 135: u"\u2021", 136: u"\u02C6",
            137: u"\u2030", 138: u"\u0160", 139: u"\u2039", 140: u"\u0152",
            142: u"\u017D", 145: u"\u2018", 146: u"\u2019", 147: u"\u201C",
            148: u"\u201D", 149: u"\u2022", 150: u"\u2013", 151: u"\u2014",
            152: u"\u02DC", 153: u"\u2122", 154: u"\u0161", 155: u"\u203A",
            156: u"\u0153", 158: u"\u017E", 159: u"\u0178"}


def replace_cp1252(input):
    def cp1252mapping(match):
        codepoint = int(match.group(1))

        if 128 <= codepoint <= 159:
            if codepoint in map1252:
                return map1252[codepoint]
            return '[?]'
        else:
            return match.group()
    return re.sub(b'&#(\d+);', cp1252mapping, input, flags=re.I)



##########################################
# html processing
##########################################

_html_body_pattern = re.compile(r'.+<BODY.*?>(.+?)</BODY>.+', re.I | re.DOTALL)


def get_soup_with_body(html):
    soup = BeautifulSoup(replace_cp1252(html), "lxml")
    if soup.body is None:
        bodymatch = re.search('(<body.+</body>)', html, re.I | re.U | re.DOTALL)
        if bodymatch:
            soup = BeautifulSoup(replace_cp1252(bodymatch.group(1)), "lxml")
        else:
            soup = None
    return soup


def remove_external_content(html, image_alt=False):
    soup = get_soup_with_body(html)
    if soup is None:
        return ''
    for tag in soup.find_all(['head', 'iframe', 'script', 'style']):
        tag.replace_with('')
    for tag in soup.find_all('img'):
        if image_alt and 'alt' in tag.attrs:
            tag.replace_with('[IMAGE {}]'.format(tag['alt']))
        else:
            tag.replace_with('[IMAGE]')
    return u'\n'.join([unicode(x) for x in soup.body.contents])


## rough approximation as to whether text is html or not
htmltags = ['<html','<body', '<div', '<span', '<br>', '<a href', '<p>', '<p ']
def is_html(text):
    return any([x in text.lower() for x in htmltags])


## The next 3 functions work together

## extracts and returns text from BeautifulSoup parts; deals with nested tags
def _get_tag_text(headtag):
    if headtag is None:
        return ''
    if isinstance(headtag, NavigableString):
        return headtag.string
    subtext = ''
    for tag in headtag.contents:
        subtext += _get_tag_text(tag)
    return subtext

## modifies BeautifulSoup components in place
def _extract_tag_text(headtag):
    if headtag is None:
        return
    if headtag.name == 'pre':
        headtag.replace_with(headtag.text)
        return
    if isinstance(headtag, NavigableString):
        headtag.replace_with(headtag.string.strip().replace('\n',' ')+'\n')
        return
    for tag in headtag.contents:
        _extract_tag_text(tag)


inline_tags=['b','i','u','em','strong','big','small','ins','del','font','a','abbr']
spacecollapse = re.compile(r'\s+',re.UNICODE)

def tag_text_to_lines(html, table_single_line=True, image_alt=False):
    '''
    Extracts the text from the body of the supplied html and puts the text into a list of lines of text.

    Does a lot of processing of the text
    * removes the html header and any iframes
    * replaces image tags with [IMAGE], or if image_alt, [IMAGE alttext]
    * content of a table is written to a single line surrounded by [TABLE START]...[TABLE END]
    * white space is collapsed
    * text is converted to unicode

    Aims to group into lines based on:
    * Content in divs and paragraphs are one line together
    * formatting tags (like b, strong, u, em, font, span, a) don't start a new line of text
    * All content for a table is in one line if table_single_line is True, else
        each row is a single line

    Note that this function obliterates a lot of the structure and spacing of the html except the specific block
    nature it's trying to preserve

    If supplied html doesn't have a a body tag, returns an empty string
    '''
    soup = BeautifulSoup(replace_cp1252(html), "lxml")
    if soup.body is None:
        bodymatch = re.search('(<body.+</body>)', html, re.I | re.U | re.DOTALL)
        if bodymatch:
            soup = BeautifulSoup(replace_cp1252(bodymatch.group(1)), "lxml")
        else:
            return ''
    ## <br> tags
    for br in soup.find_all("br"):
        br.replace_with("\n")

    ## remove external content
    for tag in soup.find_all(['head', 'iframe', 'style', 'script']):
        tag.replace_with('')

    ## images
    for tag in soup.find_all('img'):
        if image_alt and 'alt' in tag.attrs:
            tag.replace_with('[IMAGE {}]'.format(tag['alt']))
        else:
            tag.replace_with('[IMAGE]')

    ## separate superscripts with a space so they are separate words
    for tag in soup.find_all(['sup']):
        tag.replace_with(' '+tag.get_text().replace('\n',' ')+' ')

    ## inline formatting tags
    for tag in soup.find_all(inline_tags):
        tag.replace_with(_get_tag_text(tag).replace('\n', ' '))

    ## tables
    if table_single_line:
        for tag in soup.find_all('tr'):
            tag.replace_with(tag.get_text().strip().replace('\n',' ')+' ')
        for tag in soup.find_all('table'):
            tag.replace_with('[TABLE START] '+tag.get_text()+' [TABLE END]')
    else:
        for tag in soup.find_all('tr'):
            tag.replace_with(tag.get_text().strip().replace('\n',' ')+'[[TABLE ROW]]')
        for tag in soup.find_all('table'):
            tag.replace_with('[TABLE START][[TABLE ROW]] '+tag.get_text()+' [TABLE END]')

    ## now reduce tags to their text
    soup = BeautifulSoup(unicode(soup), "lxml") ## cleans up vestiges of old tag objects
    _extract_tag_text(soup.body)
    lines = []
    for line in soup.get_text().split('\n'):
        if len(line.strip()) > 0:
            tmp = spacecollapse.sub(' ', line.strip())
            if not table_single_line:
                lines += [x.strip() for x in tmp.split('[[TABLE ROW]]') if len(x.strip())>0]
            else:
                lines.append(tmp)
    return lines



#########################################
# Processing for display in a webapp
#########################################

plaintexttags = re.compile(r'<PAGE>|</?C>|</?S>|</?caption>', re.I)

## convenience processing function
def ensure_html(text):
    '''
    If supplied text is html, return the html with external content removed.
    Otherwise, strip out some tag-like markers that appear in plain-text SEC documents and wrap the
    text in <pre> tags.
    '''
    if is_html(text):
        return remove_external_content(text)
    cleaned = plaintexttags.sub('',text)
    return '<pre>'+cleaned+'</pre>'