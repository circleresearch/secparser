from setuptools import setup

setup(name='secparser',
      version='0.2',
      description='For handling SEC EDGAR filings',
      url='',
      author='Christina Maimone',
      author_email='christina.maimone@gmail.com',
      license='MIT',
      packages=['secparser'],
      install_requires=['BeautifulSoup4', 'lxml', 'jinja2'],
      zip_safe=False,
      include_package_data=True)