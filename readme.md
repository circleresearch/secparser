# Overview #

Library for processing SEC EDGAR filings into python objects (generally dictionaries).  Also some utility functions to work with the documents.  Formerly called gsbsec internally.

# Installation and Documentation #

Want to use `secparser`?  If you have access to this repository and git set up where you want to install, do:

```
pip install git+ssh://git@bitbucket.org/circleresearch/secparser.git
```

The documentation is at http://secparser.readthedocs.io/en/latest/

You can also copy the `.tar.gz` or `.zip` files from the `dist` directory and install from those.


# Development Notes #

## Directories ##

### secparser ###

This directory is the package.  The file `secdoc.py` is stand-alone at this point.  If you want to modify it, you can just copy it into your project and do so.  The `utilities.py` file is a work in progress to add some useful utilities for working with the contents of SecDocument objects; mostly useful for html processing.  `form4.py` has the Form4 object; it requires some xml schema files as input, and an html template.  It also uses functions from `utilities.py` so it is not stand-alone.

### docs ###

Sphinx documentation.  To build the documentation (for a local copy), get Sphinx via `pip install sphinx`.  Then in the docs directory, `make html` to make an html version of the documentation.  It will appear in the `_build/_html` directory.   Open `index.html` for the main page.

All of the documentation is written in the `*.rst` files.  It is not built from comments in the code itself.  The formatting of the documentation generated from the comments in the code didn't look great when we tried it, and there are a number of methods and attributes that we don't want to document.  Writing restructured text files directly seemed easier, but this is perhaps something to look into in the future.

The documentation is at http://secparser.readthedocs.io/en/latest/. It should update automatically on pushes to the repository. 


### test ###

`testscript.py` has some basic tests.  Main approach is to process random documents without error, and then check a few known odd documents.  It's not a completely sufficient test, but any of the test methods should run without error.  Run on the yens for access to the documents.  Testing is an area that needs some work.



## Creating installable package ##

Edit `setup.py`, particularly the version number, as needed.

From the main directory, run

```
python setup.py sdist --formats=gztar,zip
```

So there's a Windows file to share.

Push to bitbucket, proceed like above.