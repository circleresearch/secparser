############
Introduction to secparser
############

The :class:`SecFiling` class processes SEC filings from the `EDGAR system <https://www.sec.gov/edgar/searchedgar/companysearch.html>`_ into Python objects from which you can easily extract all attributes/metadata from the form, the associated companies (owners, filers, reporting owners, issuers etc.), and any documents or images attached to the form. The :class:`SecFiling` class takes as input a "Complete submission text file" version of a filing.  This is a plain text file that contains in it all of the documents submitted as a part of the filing, along with metadata about the filing.  See this `example <https://www.sec.gov/Archives/edgar/data/320193/000119312516559625/0001193125-16-559625.txt>`_ of a filing.

This plain text input file is processed into a :class:`SecFiling` object that has a list of :class:`SecCompany` objects, a list of :class:`SecDocument` objects, and a :class:`dict` of metadata information from the form header.

:class:`SecDocument` and :class:`SecCompany` are not designed to be created directly, but there's nothing stopping you from doing so.

Installation
=================

You can install the package with ``pip``.  You'll need ``git`` and ``ssh`` installed on your system::

   pip install git+ssh://git@bitbucket.org/circleresearch/secparser.git


Alternatively, you can download a copy of the package from the ``dist`` directory in the `BitBucket repository <https://bitbucket.org/circleresearch/secparser/src>`_ and then install from your local copy.

If you want to add the package to a requirements.txt file, add the line::

    git+ssh://git@bitbucket.org/circleresearch/secparser.git#egg=secparser

