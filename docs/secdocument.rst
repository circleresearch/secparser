########################
The SecDocument Class
########################

:class:`SecDocument` contains information about a particular document associated with a given form. These are generated and accessed from the :class:`SecFiling` class although can be generated manually using the original filename and raw text content if necessary.

This is the generic version of a document that can be used for any type with any contents.  :class:`Form4` can be used to process the :attr:`text` of a :class:`SecDocument` if appropriate.


.. class:: SecDocument(header, text)

      :class:`SecDocument` is the class for the document object. Certain parameters are accessible such as the filename, the contents and the original form type.

      :param str contents: The raw text content of the document: what's between the <DOCUMENT> </DOCUMENT> tags

      .. attribute:: metadata

         A :class:`dict`. Usually contains keys for: TYPE (entered by the filer, can have typos), DESCRIPTION (entered by the filer), FILENAME (original name of the file as submitted), SEQUENCE (where it appears in the file).  Value for SEQUENCE is converted to an int.

      .. attribute:: text

         Raw text of the document object.  The content between the <TEXT> </TEXT> tags.  May be plain text, but may also be html, xml, or an encoded binary file type (jpg, pdf, etc.).

      .. method:: to_dict(include_text=False)

         This converts the entire document into a :class:`dict`; primarily for use by :meth:`SecFiling.to_json`.  `include_text` determines whether the text of the document is included in the object or not.

      .. method:: uu2string(mode=None)

         Sometimes documents are binary files encoded as text (uuencoded).  This method converts the text string back to its binary format.  `mode` is an optional paramenter to :func:`uu.decode`

      .. method:: to_file(path_to_save, filename, save_file)

         This method saves documents to a file, it supports uuencoded and non uuencoded files and saves them with the original filename as specified in the :attr:`metadata` (``metadata['FILENAME']``). Filename parameter defaults to original image name, path defaults to current working directory. If false is provided no file is saved, document is simply returned as a string.

Extracting Binary Files
==========================

Using :meth:`SecDocument.to_file` we can decode plaintext files or uuencoded jpeg files. We can look at the list of documents associated with our form by looking at the :class:`SecFiling` object.

.. code-block:: python
   :linenos:

   import pprint
   form = '1355451_4_0001355451-07-000035.txt'
   with open(form, 'rb') as unzipped_form:
      form_object = SecFiling(form, unzipped_form.read())
      pprint.pprint(form_object.documents)

.. code-block:: none

   [SecDocument; FILENAME: form8a.htm; TYPE: 8-A12G; DESCRIPTION: FORM 8-A12G; SEQUENCE: 1,
   SecDocument; FILENAME: vcert1.jpg; TYPE: GRAPHIC; SEQUENCE: 2,
   SecDocument; FILENAME: vcert2.jpg; TYPE: GRAPHIC; SEQUENCE: 3,
   SecDocument; FILENAME: ex_4-1.htm; TYPE: EX-4.1; DESCRIPTION: SPECIMEN STOCK CERTIFICATE 1; SEQUENCE: 4,
   SecDocument; FILENAME: ex_4-2.htm; TYPE: EX-4.2; DESCRIPTION: SPECIMEN STOCK CERTIFICATE 2; SEQUENCE: 5]

Here we can see a list containing all the :class:`SecDocument`s associated with our form. We have 2 image files associated with our form. We want to decode vcert1.jpg, which is the second item in the list. The following code will save that image to the desktop.

.. code-block:: python
   :linenos:
   
   form_object.documents[1].to_file(path='/Users/username/Desktop/', filename='image.jpg')

Which saves the following image to our desktop.  

.. figure:: _static/image.jpg
   :width: 300px
   :alt: Example Image