
secparser documentation
==================================

.. toctree::
   :maxdepth: 2

   introduction
   secfiling
   seccompany
   secdocument
   form4
   utilities

Bugs? Issues? `Contact Christina`_

.. _Contact Christina: christina.maimone@gmail.com



