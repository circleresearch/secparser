########################
utilities module
########################

Some random methods that might come in handy for working with the data.

.. method:: pad_cik(cik)

   CIKs (Central Index Keys) can have leading zeros.  These can get lost if the CIK gets processed as a number instead of a string.  They are also dropped in the EDGAR filenames.  This function will add the appropriate number of leading zeros back on and return a string.

.. method:: original_url(filename)

   This method is specific to the private `GSB <http://gsb.stanford.edu>`_ infrastructure.  It will take the filename, with or without the path, for a filing as stored on the yen servers and return the url for the filing on the SEC website.

.. method:: core_filename(filename)

   This method is specific to the private `GSB <http://gsb.stanford.edu>`_ infrastructure.  It will take the filename, with or without the path, for a filing as stored on the yen servers and return the accession number part.

.. method:: is_xml(text)

    Does the supplied text start with a XML tag (``<xml`` or ``<?xml``)?

.. method:: replace_cp1252(input)

   Takes a `input` string and replaces Windows-based CP1252 characters with their unicode equivalent.  Useful for cleaning up the content of the SEC filings, particularly html documents.  Gets rid of funky curly quotes and the like.

.. method:: remove_external_content(html, image_alt=False)

   Supplied with a html string, it creates a copy and removes 'head', 'iframe', 'script', 'style' tags, and replaces 'img' tags with either ``[IMAGE]`` or ``[IMAGE This is the alt content]`` depending on `image_alt`.  The contents of the body of the resulting html document is returned, so that it can be added to a ``<div>`` or other html container.

.. method:: is_html(text)

   A rough approximation for detecting wither supplied text is html or not based on whether common html tags are present.

.. method:: tag_text_to_lines(html, table_single_line=True, image_alt=False)

   Extracts the text from the ``<body>`` of the supplied html and puts the text into a list of lines of text.

   Does a lot of processing of the text:

   * removes the html header and any iframes
   * replaces image tags with ``[IMAGE]``, or if `image_alt`, ``[IMAGE alttext]``
   * content of a table is written to a single line surrounded by ``[TABLE START]`` and ``[TABLE END]``
   * white space (all types) is collapsed to a single space
   * text is converted to unicode

   Aims to group content into lines of text based on:

   * Content in the lowest nested level of a ``<div></div>`` or ``<p></p>`` are one line together
   * Formatting tags (like ``<b>``, ``<strong>``, ``<u>``, ``<em>``, ``<font>``, ``<span>``, ``<a ..>``) don't start a new line of text
   * All content for a ``<table>`` is in one line if `table_single_line` is True, else each row is a single line

   Note that this function obliterates a lot of the structure and spacing of the html except the specific block nature it's trying to preserve

   If supplied html doesn't have a ``<body>`` tag, returns an empty string.

   This function works well for pre-processing the text of an html SEC filing that contains freely-written text in many cases, but it may not be the right choice for your circumstances.

.. method:: ensure_html(text)

   If supplied text :meth:`is_html`, return :meth:`remove_external_content` applied to the text. Otherwise, strip out some tag-like markers that appear in plain-text SEC documents and wrap the text in ``<pre>`` tags.
