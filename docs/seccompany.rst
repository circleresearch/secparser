########################
The SecCompany Class
########################

:class:`SecCompany` objects are generated and accessed from the :class:`SecFiling` class and would not generally be initiated manually.

The :class:`SecCompany` contains information about a particular company associated with a given filing. The possible association types are:

- SERIAL COMPANY
- FILER
- FILED BY
- FILED FOR
- REPORTING OWNER
- ISSUER
- SUBJECT COMPANY

Company data for a single company of type FILER looks like::

    FILER:

        COMPANY DATA:
            COMPANY CONFORMED NAME:		APPLE INC
            CENTRAL INDEX KEY:			0000320193
            STANDARD INDUSTRIAL CLASSIFICATION:	ELECTRONIC COMPUTERS [3571]
            IRS NUMBER:				942404110
            STATE OF INCORPORATION:		CA
            FISCAL YEAR END:			0924

        FILING VALUES:
            FORM TYPE:		10-Q
            SEC ACT:		1934 Act
            SEC FILE NUMBER:	001-36743
            FILM NUMBER:	161595799

        BUSINESS ADDRESS:
            STREET 1:		        ONE INFINITE LOOP
            CITY:			CUPERTINO
            STATE:			CA
            ZIP:			95014
            BUSINESS PHONE:		(408) 996-1010

        MAIL ADDRESS:
            STREET 1:		        ONE INFINITE LOOP
            CITY:			CUPERTINO
            STATE:			CA
            ZIP:			95014

        FORMER COMPANY:
            FORMER CONFORMED NAME:	APPLE COMPUTER INC
            DATE OF NAME CHANGE:	19970808




.. class:: SecCompany(line_data, company_type)

      :class:`SecCompany` is the class for the company object.

      :param str line_data: The lines to be used from the form to generate the object
      :param str company_type: The company type (SERIAL COMPANY, FILER, FILED BY, FILED FOR, REPORTING OWNER, ISSUER, SUBJECT COMPANY)

      .. attribute:: type

         A string with the type as supplied when the object was created.  See possible values above.

      .. attribute:: metadata

         A :class:`dict` containing the information about the company from the COMPANY DATA section. Fields can include COMPANY CONFORMED NAME, CENTRAL INDEX KEY, STANDARD INDUSTRIAL CLASSIFICATION, IRS NUMBER, STATE OF INCORPORATION, FISCAL YEAR END (converted to :class:`datetime.date` if the value matched expected date pattern).

      .. attribute:: sic

	     Integer containing the value for the Standard Industrial Classification. The full description can be found under
         ``metadata['STANDARD INDUSTRIAL CLASSIFICATION']`` however the integer value can be useful for determining the company's major classification and its subgroup within that. More information on SIC codes can be found at http://unstats.un.org/unsd/publication/seriesM/seriesm_4rev4e.pdf.  ``None`` if there is no sic.

      .. attribute:: cik

	     String value of Central Index Key.  Can also be accessed through the ``metadata['CENTRAL INDEX KEY']``, but it's used frequently enough that it's been pulled out separately for convenience.  ``None`` if for some strange reason there is no CIK.

      .. attribute:: filing_values

         A :class:`dict` containing data from the FILING VALUES section, including fields like FORM TYPE, SEC ACT, SEC FILE NUMBER

      .. attribute:: addresses

         List containing :class:`Address` objects. Usually one or both of BUSINESS or MAIL addresses depending on which ones were included on the form.

      .. attribute:: former_companies

         List containing former names of the company if there were any. For each former name there is a A :class:`dict` containing key value pairs for FORMER CONFORMED NAME and DATE OF NAME CHANGE.

      .. method:: to_dict()

         This converts the information for the company into a :class:`dict`; primarily for use by :meth:`SecFiling.to_json`.

########################
Address
########################

For storing addresses with :class:`SecCompany`.  Addresses are initialized with just their type, and the :attr:`~Address.fields` member :class:`dict` is created empty; it can be edited directly later.

.. class:: Address(address_type)

    :param str address_type: type of address, generally BUSINESS or MAIL

    .. attribute:: type

    The type string supplied when the :class:`Address` was created.

    .. attribute:: fields

    A :class:`dict` you can store address fields in.  Key will be used as a label.

    .. method:: pretty():

    A pretty formatted version of the address, along the lines of:

    .. code-block:: none

        STREET 1       10 S DEARBORN ST 37TH FLR
        STREET 2       PO BOX A-3005
        CITY           CHICAGO
        STATE          IL
        ZIP            60690-3005
        BUSINESS PHONE 3123947399


Example: Obtaining Mailing Address
===================================

Using :attr:`SecCompany.addresses` we can easily extract the addresses associated with the company. In this example

.. code-block:: python
   :linenos:

   form = '18568_3_0001183733-03-000245.txt'
   with open(form, 'r') as form_file:
      form_object = SecFiling(form, form_file.read())
      print form_object.companies
 

.. code-block:: none
  
  [ISSUER: REPUBLIC SERVICES INC, REPORTING OWNER: Serianni Charles F]

This is suggesting our form has an Issuer and a Reporting Owner associated with it. If we take a closer look at the available addresses of the reporting owner using the following command

.. code-block:: python
   :linenos:

   print form_object.companies[1].addresses


.. code-block:: none
   
   [BUSINESS ADDRESS, MAIL ADDRESS]

.. code-block:: python
   :linenos:

   print form_object.companies[1].addresses[1].pretty()


.. code-block:: none

    STREET 1    15 SOUTH 20TH ST
    STREET 2    P O BOX 10566
    CITY        BIRMINGHAM
    STATE       AL
    ZIP         35233
