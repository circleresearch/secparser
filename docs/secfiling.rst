########################
The SecFiling Class
########################

The :class:`SecFiling` class stores metadata relevant to the entire filing, as well as the :attr:`companies` and :attr:`documents` that compose the filing. You provide the original filename, and the contents.  Then you can use the :class:`SecFiling` class to extract fields from the form header and get the individual documents as :class:`SecDocument` objects. See below for examples.

.. class:: SecFiling(filename, contents)

      :class:`SecFiling` is the object for the entire filing and contains all of the information from the supplied contents.

      :param str filename: The filename of the form, as stored on your local system.  Can be ``None`` or an empty string if you don't want to supply it, but it's often useful to have.
      :param str contents: The raw text of the form as a string.

      .. attribute:: filename

         A string with the filename supplied when the object was created.

      .. attribute:: metadata

         :class:`dict` containing metadata extracted from the form. Common keys include:

         - SEC-DOCUMENT
         - SEC-HEADER
         - ACCEPTANCE DATETIME
         - ACCESSION NUMBER
         - CONFORMED PERIOD OF REPORT
         - CONFORMED SUBMISSION TYPE
         - FILED AS OF DATE
         - PUBLIC DOCUMENT COUNT
         - DATE AS OF CHANGE

         but any other fields that exist at the top of the filing are included (for example, ITEM INFORMATION for Form 8-K).  The dictionary keys are the field labels as they appear in the filing, minus any surrounding <> or ending :.  See examples below.  Fields known to be dates or datetimes (ACCEPTANCE DATETIME, CONFORMED PERIOD OF REPORT, FILED AS OF DATE, DATE AS OF CHANGE) have been converted to :mod:`datetime` objects if their values conformed to expectations (otherwise left as is).  Fields known to be integers (PUBLIC DOCUMENT COUNT) have been converted to ints.

      .. attribute:: date

         The FILED AS OF DATE as a python :class:`datetime.date` object.

      .. attribute:: type

         The CONFORMED SUBMISSION TYPE (the name of the form filed).

      .. attribute:: companies

         List containing accessible :class:`SecCompany` objects associated with the filing. The type of each company (SERIAL COMPANY, FILER, FILED BY, FILED FOR, REPORTING OWNER, ISSUER, SUBJECT COMPANY) is stored in the :class:`SecCompany` object.

      .. attribute:: documents

         List containing accessible :class:`SecDocument` objects associated with the filing.  If a document is detected to be of a known specialized type, it will create objects that are the appropriate subclass of :class:`SecDocument`.  Currently only :class:`Form4` is implemented to handle .

      .. method:: to_dict(include_text=False)

         This converts the entire filing into a :class:`dict`; primarily for use by :meth:`to_json()`.  Dates are converted to their ISO format, other values to strings.  `include_text` determines whether the content of the documents -- text, html, xml, binary, or otherwise -- is included in the object or not.
         This converts the entire filing into a :class:`dict`; primarily for use by :meth:`to_json()`.  Dates are converted to their ISO format, other values to strings.  `include_text` determines whether the content of the documents -- text, html, xml, binary, or otherwise -- is included in the object or not.

      .. method:: to_json(path="", filename=None, save_file=True, include_text=False)

         A JSON version of what's produced by :meth:`to_dict`.  By default, saves the file to :attr:`self.filename` in the current working directory if `save_file` is True.  Returns the JSON version as a string. `include_text` determines whether the content of the documents is included in the object or not.


Example: Extracting Date
===========================


Using the :attr:`date` member in :class:`SecFiling` we can easily find out when the form was filed

.. code-block:: python
   :linenos:

   >> form = '18568_3_0001183733-03-000245.txt'
   >> with open(form, 'r') as form_file:
   >> 	form_object = SecFiling(form, form_file.read())
   >>	print form_object.date
 

.. code-block:: none
  
   2003-09-29

Example: Inspecting Metadata
===================================

Using the :attr:`metadata` member in :class:`SecFiling` we can easily print the metadata associated with the form

.. code-block:: python
   :linenos:

   import pprint
   form = '18568_3_0001183733-03-000245.txt'
   with open(form, 'r') as form_file:
      form_object = SecFiling(form, form_file.read())
      pprint.pprint(form_object.metadata)
 

.. code-block:: none
  
   {'ACCEPTANCE-DATETIME': '20030929181436',
   'ACCESSION NUMBER': '0001183733-03-000245',
   'CONFORMED PERIOD OF REPORT': '20030901',
   'CONFORMED SUBMISSION TYPE': '3/A',
   'FILED AS OF DATE': '20030929',
   'PUBLIC DOCUMENT COUNT': '1',
   'SEC-HEADER': '0001183733-03-000245.hdr.sgml : 20030929'}

Here we can see the :attr:`metadata` contains additional information which maybe useful for analysis such as report period, accession number, etc.

Generating JSON form
===================================

Using :meth:`SecFiling.to_json` we can take a raw form and create a structured JSON file which can be used in a database or other analysis.

.. code-block:: python
   :linenos:

   form = '1355451_4_0001355451-07-000035.txt'
   with open(form, 'r') as form_file:
      form_object = SecFiling(form, form_file.read())
      form_json = form_object.to_json()
      with open('/Users/username/Desktop/form.json', 'w') as json_file:
         json_file.write(form_json)

This will create a new file on the desktop called form.json, containing a structured json version of the form

