########################
The Form4 Class
########################

The :class:`Form4` class will parse data out of a recent XML version of a Form 4 or Form 4/A.  It does not currently work with older text format Form 4 documents, html versions of the Form, or older XML schemas (see `Note about XML Schemas`_). For newer filings, EDGAR usually provides Form 4 filings in both XML and HTML format, but it is the XML format that is included as part of the "Complete submission text file" version of a filing.  This class can extract key information such as owner information and transaction details, as well as convert the XML form to HTML5/CSS for a quick overview. See examples below for how to use this class.

NOTE: This class is believed to work correctly, but use with some caution.  People can do a lot of strange things in filing forms, and we may not have caught all of the special cases yet.

Supporting parsing of older XML and the HTML version of the forms is on the to-do list.

.. class:: Form4(xml)

      :param str xml: a string with XML, as from a :class:`SecDocument` (:attr:`SecDocument.text`).  :class:`Form4` doesn't store the raw text.  When the :class:`Form4` is created, it checks that the XML is a Form 4 or Form 4/A, that the content is XML, and that the schema version is compatible with what :class:`Form4` knows how to handle.  If the XML does not have content that can be parsed, an :class:`Exception` is raised.  Check the :class:`Exception` message:

        * "Supplied data is not XML": self-explanatory
        * "Unknown document type; no schema available": the supplied XML doesn't contain a Form 4 or Form 4/A
        * "Could not parse XML": there was something wrong with the formatting of the XML separate from any issues with the schema.
        * "Incompatible schemaVersion: cannot parse": the XML uses a schema that the class does not currently know how to handle.

      For a few of the older schemas, :class:`Form4` can parse them, but there are a few different tags.  In such cases, a :class:`Warning` will be issued, but the :class:`Form4` may still be created.

      .. attribute:: tree

         This is the XML tree of the form, as an :class:`lxml.etree`. If the form conforms to the SEC standards it is not necessary to access this member, however if it gives warnings about tags not included they can be accessed via this object. See the LXML http://lxml.de documentation for more details on using LXML trees.

      .. attribute:: transactions

         A :class:`dict` where the keys are transaction types ("nonDerivativeTransaction", "nonDerivativeHolding", "derivativeTransaction", "derivativeHolding") and values are lists of transactions, each represented by a :class:`dict`.  Each transaction :class:`dict` has keys from the XML tags and values are :class:`Entry` objects (a class defined in the ``form4`` module that holds the tag value and footnotes).  :attr:`transactions` is not intended to be used directly, but if you want to access footnotes associated with specific fields, then it's an option.  Alternatively, you could use the :attr:`tree` directly.  See below for an example of using :attr:`transactions`.

      .. attribute:: footnotes

         A :class:`dict` with keys as the footnote numbers (F1, F2, F3, etc.) and values the footnote element from the XML :attr:`tree`.  See below for example of accessing the footnotes.

      .. attribute:: issuer

         :class:`dict` with keys "issuerCik", "issuerName", "issuerTradingSymbol"

      .. attribute:: ownerSignature

         A list of :class:`dict` with keys "signatureName", "signatureDate"

      .. attribute:: reportingOwner

         A list of reporting owners, each is a :class:`dict` with keys for the tags that were found in the XML

      .. attribute:: otherdata

         A :class:`dict` with keys 'schemaVersion', 'documentType', 'periodOfReport', 'remarks', 'notSubjectToSection16', if they were available in the XML

      .. method:: to_dict(include_footnotes=True)

         A :class:`dict` with all of the data from the object.  Uses :meth:`get_transactions()` (see below) to format the transactions data.

      .. method:: get_transactions()

         Returns a list of transactions, where each transaction is a :class:`dict` that has flat values -- no lists or dictionaries nested.  The type of the transaction is included with key ``type`` and footnotes are aggregated at the end with key ``footnotes``.  If you want the association between footnotes and individual pieces of data, use :attr:`transactions` instead.

      .. method:: to_html()

         Creates an HTML document with the XML data from the form filled in, similar to the HTML version of the data provided in SEC EDGAR.  Returns the complete HTML as a string.

Note about XML Schemas
=========================

Individual Form 4 and Form 4/A XML files have schema version numbers in them.  The only 2 versions that could be located on the SEC website were version 2 (https://www.sec.gov/info/edgar/ownershipxmltechspec-v2.htm) and the current version 5.1 (https://www.sec.gov/info/edgar/ownershipxmltechspec.htm).  These correspond to:

* Version 2

  * schemaVersion X0303 for Form 4/A
  * schemaVersion X0306 for Form 4
  * schemaVersion X0304 for the common ownership document

* Version 5.1

  * schemaVersion X0306 for Form 4/A
  * schemaVersion X0306 for Form 4
  * schemaVersion X0304 for the common ownership document

So only the schema version for Form 4/A changed.  But comparing the schema definition files, there is actually no change except the name of the version.  So we're using the version 5.1 files here for everything instead of trying to match the schema version given in the actual filing.

Previous versions of the schema do appear to have had some different fields.  See documentation on Version 1 (https://www.sec.gov/info/edgar/ownershipxmlspec-v1-r1.doc), but unfortunately we can't locate the schema definition file to go with this documentation.  Schema version X0101 or X0201 (for which documentation was not located) are known not to be consistent with the Version 2 schema and cannot be handled by this class.  Any Form 4 or Form 4/A that lists one of these schemas will result in an Exception being raised during object creation.  Schema versions X0202 and onward seem to work ok, but you may want to do some of your own verification for older schemas.  X0202 is common, but the definition file could not be located.

Roughly, filings from 2002 and before tend not to be XML, and ones from 2003 and 2004 tend to be older schemas that are not handled by :class:`Form4`.  There are some filings from around 2003 where the schemaVersion isn't specified.  These have a slightly different structure for the transaction data, but :class:`Form4` tries to handle them.  Specifically, ``derivativeSecurity`` is mapped to ``derivativeTransaction`` and ``nonDerivativeSecurity`` is mapped to ``nonDerivativeTransaction``.  The older tags include both holdings and transactions, but since they can't be distinguished ahead of time, the transaction labels are used because the set of tags for transactions is a superset of the set of tags for holdings.

This class will do its best to parse the XML according to the schema.  It will generally only provide warnings when the schema is violated.  If it cannot parse the XML even without the schema, then an excpetion will be raised.


Example: Creating a Form4 object
===================================

If we have a :class:`SecFiling` object ``filing``

.. code-block:: python
    :linenos:

     for doc in filing.documents:
        if doc.metadata['TYPE'] == '4/A' or doc.metadata['TYPE'] == '4':
            try:
                form4 = Form4(doc.text)
            except Exception as e:
                print "Problem making Form4: {}".format(e.message)


Generating pandas DataFrame or CSV
===================================

If you want to export the transaction data to a data set, ``pandas`` can be useful.

.. code-block:: python
    :linenos:

     import pandas as pd
     df = pd.DataFrame.from_dict(form4.get_transactions())
     df.to_csv() ## to get a csv version

But you may want to add some more data from the filing into the data set beyond just the transaction data.  You could make a :class:`dict` for the other form-level information and update each transaction :class:`dict` with the form :class:`dict` to get those columns added in.


Generating HTML Form
===================================

Using :meth:`to_html` we can see the transactions presented in a HTML format with all footnotes and remarks for a quick overview of the form. A full and complete html file is generated.  The HTML template is taken from the SEC EDGAR website, with OMB information removed.  There are some small formatting differences from how the forms are displayed on the SEC EDGAR website.

.. code-block:: python
   :linenos:

     with open('myform4.html', 'w') as of:
        of.write(form4.to_html())

`Example <_static/exampleform4.html>`_

Using :attr:`transactions` and :attr:`footnotes`
=======================================================

If you need detailed footnote information about transactions, one option is to use :attr:`transactions` directly.  You can access the footnote text from :attr:`footnotes`.

.. code-block:: python
   :linenos:

     for transactiontype, typetransactions in form4.transactions.items():
        print transactiontype
        for trans in typetransactions:
            for key, entry in trans.items():
                print "  {}: {}, {}".format(key, entry.value, entry.footnotes)
                if entry.footnotes is not None:
                    for fn in form4.footnotes:
                        print "   {}: {}".format(fn, self.footnotes[fn])



